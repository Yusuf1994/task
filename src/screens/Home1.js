import React from 'react';
import { ImageBackground, View, Dimensions, Image, Text, TouchableOpacity, Alert } from 'react-native';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
export default class Home1 extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);
    }
    render() {
        return (
            <TouchableOpacity style={{ width: width }}
                onPress={() => {
                     Alert.alert('Go to booking page')
                    // this.props.rowClick(this.props.column_data)
                }}>
                <View style={{ width: width,padding: 10 }}>

                    <View style={{ width: '100%',}}>
                        <ImageBackground style={{ width: '100%',  height: 200 }}
                            resizeMode='cover'
                            source={this.props.column_data.image}>
                            <View style={{ flex: 1 }}></View>
                            <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row', justifyContent: 'space-between', width: '100%', alignItems: 'center', padding: 10 }}>

                                <View style={{ alignItems: 'center' }}>
                                    <Text style={{ color: 'black', fontSize: 30, fontWeight: 'bold' }}>
                                        {this.props.column_data.carname}
                                    </Text>
                                    <Text style={{ color: 'black' }}>
                                        {this.props.column_data.carmodel}
                                    </Text>
                                </View>

                                <TouchableOpacity style={{ width: 30, height: 30 }}
                                    onPress={() => {
                                        Alert.alert('Go to Menu')
                                    }}
                                >
                                    <Image style={{ width: 30, height: 30 }}
                                        resizeMode='contain'

                                        source={require('./../../assets/ic_stat_menu.png')}>

                                    </Image>
                                </TouchableOpacity>
                            </View>
                        </ImageBackground>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}