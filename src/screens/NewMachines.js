import React from 'react';
import { Alert, Dimensions, Image, SafeAreaView, Text, TextInput, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
export default class NewMachines extends React.Component {
    render() {
        return (
            <SafeAreaView style={{ width: width, height: height, flex: 1, backgroundColor: 'black', padding: 10 }}>
                <View style={{ width: '100%', height: '25%', backgroundColor: 'gray', borderRadius: 10,justifyContent:'center',alignItems:'center' }}>
                    <TouchableOpacity onPress={() => {
                        Alert.alert('Add Image')
                    }}>
                        <Image style={{width:100,height:50}}
                        resizeMode='contain'
                         source={require('./../../assets/camera.png')} />
                    </TouchableOpacity>
                </View>
                <Text style={{ color: '#FB5908', marginTop: 10, fontSize: 15 }}>
                    {'Add Machine Make'}
                </Text>
                <TextInput style={{ color: 'white', fontSize: 13 }}
                    placeholder={'Add Machine Make'}
                    placeholderTextColor={'gray'} />
                <View style={{ width: '100%', height: 2, backgroundColor: 'gray' }} />
                <Text style={{ color: '#FB5908', marginTop: 10, fontSize: 15 }}>
                    {'Add Machine Model'}
                </Text>
                <TextInput style={{ color: 'white', fontSize: 13 }}
                    placeholder={'Add Machine Model'}
                    placeholderTextColor={'gray'} />
                <View style={{ width: '100%', height: 2, backgroundColor: 'gray' }} />
                <Text style={{ color: '#FB5908', marginTop: 10, fontSize: 15 }}>
                    {'Add Machine Type'}
                </Text>
                <TextInput style={{ color: 'white', fontSize: 13 }}
                    placeholder={'Add Machine Type'}
                    placeholderTextColor={'gray'} />
                <View style={{ width: '100%', height: 2, backgroundColor: 'gray' }} />
                <Text style={{ color: '#FB5908', marginTop: 10, fontSize: 15 }}>
                    {'Add Machine Number'}
                </Text>
                <TextInput style={{ color: 'white', fontSize: 13 }}
                    placeholder={'Add Machine Number'}
                    placeholderTextColor={'gray'} />
                <View style={{ width: '100%', height: 2, backgroundColor: 'gray' }} />
                <Text style={{ color: '#FB5908', marginTop: 10, fontSize: 15 }}>
                    {'Add Machine Fuel Type'}
                </Text>
                <TextInput style={{ color: 'white', fontSize: 13 }}
                    placeholder={'Add Machine Fuel Type'}
                    placeholderTextColor={'gray'} />
                <View style={{ width: '100%', height: 2, backgroundColor: 'gray' }} />
                <Text style={{ color: '#FB5908', marginTop: 10, fontSize: 15 }}>
                    {'Add Machine Quantity'}
                </Text>
                <TextInput style={{ color: 'white', fontSize: 13 }}
                    placeholder={'Add Machine Quantity'}
                    placeholderTextColor={'gray'} />
                <View style={{ width: '100%', height: 2, backgroundColor: 'gray' }} />
                <View style={{ width: '100%' }}>
                    <TouchableOpacity
                        onPress={() => {
                            Alert.alert('Machine added')
                        }}
                        style={{ width: '80%', height: 50, backgroundColor: '#08FBE7', marginTop: 10, borderRadius: 10, justifyContent: 'center', alignSelf: 'center' }}>
                        <Text style={{ fontSize: 20, alignSelf: 'center' }}> {'Save Machine Info'} </Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}