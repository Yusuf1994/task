import React from "react";
import { Dimensions, Text, View } from 'react-native';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
export default class SplashScreen extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.navigate('Home')
        },3000);
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ width: width, height: height, flex: 1, backgroundColor: 'black', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}> {'T A S K'} </Text>
                </View>
            </View>

        )
    }
}
// componentDidMount() {
//     setTimeout(() => {
//         this.props.navigation.navigate('Home')
//     }, 3000);
// }