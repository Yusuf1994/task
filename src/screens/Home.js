import React from 'react';
import { Alert, Dimensions, FlatList, Image, SafeAreaView, Text, TouchableOpacity, View } from 'react-native';
import Home1 from './Home1';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            car_data: [
                {
                    image: require('./../../assets/car.jpg'),
                    carname: 'BMW',
                    carmodel: 'FYYD 6778'
                },
                {
                    image: require('./../../assets/car.jpg'),
                    carname: 'BMW',
                    carmodel: 'FYYD 6778'
                },
                {
                    image: require('./../../assets/car.jpg'),
                    carname: 'BMW',
                    carmodel: 'FYYD 6778'
                },
                {
                    image: require('./../../assets/car.jpg'),
                    carname: 'BMW',
                    carmodel: 'FYYD 6778'
                },
                {
                    image: require('./../../assets/car.jpg'),
                    carname: 'BMW',
                    carmodel: 'FYYD 6778'
                },
                {
                    image: require('./../../assets/car.jpg'),
                    carname: 'BMW',
                    carmodel: 'FYYD 6778'
                },
            ]
        }
    }
    _rowClick = (item) => {
        this.props.navigation.navigate("Home1")
    }
    _renderItem = (item) => {
        return <Home1
            // navigation={this.props.navigate}
            column_data={item.item}
            // car_data={this.state.car_data}
            rowClick={this._rowClick}
        />


    }
    render() {
        return (
            <SafeAreaView style={{ width: width, height: height, flex: 1, backgroundColor: 'black' }}>
                <View style={{ width: '100%', height: '10%', backgroundColor: '#17202A', justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', padding: 10 }}>
                    <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 20 }}>
                        {'My Machienes'}
                    </Text>
                    <TouchableOpacity
                        onPress={() => {
                            // Alert.alert('Add Machines')
                            this.props.navigation.navigate('NewMachines')
                        }}>
                        <Image style={{ width: 40, height: 40 }}
                            resizeMode='contain'
                            source={require('./../../assets/add.png')} />
                    </TouchableOpacity>
                </View>
                <Text style={{ color: 'gray', margin: 10 }}> {'Featured Cars'} </Text>
                <View style={{ flex: 1 }}>

                    <FlatList

                        data={this.state.car_data}
                        renderItem={this._renderItem}
                        // onRefresh={() => this.onRefresh()}
                        // refreshing={this.state.isRefreshing}
                        keyExtractor={(item, index) => index.toString()}


                    />

                </View>
            </SafeAreaView>
        )
    }
}