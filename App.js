import React from "react";
import { Text, View,Dimensions } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from "./src/screens/SplashScreen";
import Home from "./src/screens/Home";
import Home1 from "./src/screens/Home1";
import NewMachines from "./src/screens/NewMachines";
const Stack=createStackNavigator();
export default class App extends React.Component {
  render() {
    return (
      // <Text style={{color:'black'}}> {'How r u'} </Text>
      <View style={{ flex: 1 }}>
        {/* <Home1/> */}
        <NavigationContainer>
          <Stack.Navigator   >
            <Stack.Screen name='SplashScreen' component={SplashScreen} options={{headerShown:false}} />
            <Stack.Screen name='Home' component={Home} options={{headerShown:false}} />
            <Stack.Screen name='Home1' component={Home1} options={{headerShown:false}} />
            <Stack.Screen name='NewMachines' component={NewMachines} options={{headerShown:false}}/>
          </Stack.Navigator>
        </NavigationContainer>
      </View>
    )
  }
}